﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Mondol_cloth_store
{
    public partial class addItem : Form
    {
        public addItem()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            var name = Named.Text;
            var type = Type.Text;
            var price = Price.Text;
            var sellPrice = SellPrice.Text;

            DbConnect db = new DbConnect();
            string query = "INSERT INTO mcs_item (name, type, price, sell_price) VALUES('" + name + "', '" + type + "', '" + price + "', '" + sellPrice + "')";

            //open connection
            if (db.connect() == true)
            {
                var conn = db.getDB();
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, conn);

                //Execute command
                cmd.ExecuteNonQuery();
            }

            MessageBox.Show("Item added successfully...");
            this.Close();
        }
    }
}
