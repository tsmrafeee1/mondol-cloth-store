﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Mondol_cloth_store
{
    public partial class allItems : Form
    {
        public allItems()
        {
            InitializeComponent();
        }

        private void allItems_Load(object sender, EventArgs e)
        {
            DbConnect db = new DbConnect();
            if (db.connect() == true)
            {
                var conn = db.getDB();
                //create command and assign the query and connection from the constructor
                var mySqlDataAdapter = new MySqlDataAdapter("select name, type, price, sell_price from mcs_item", conn);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);
                dataGridView1.DataSource = DS.Tables[0];
            }
            
        }
    }
}
