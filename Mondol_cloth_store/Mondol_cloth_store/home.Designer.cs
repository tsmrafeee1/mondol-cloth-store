﻿namespace Mondol_cloth_store
{
    partial class home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.ViewSalesMen = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SoldItems = new System.Windows.Forms.Button();
            this.MonthlyProfit = new System.Windows.Forms.Button();
            this.TodaysProfit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Logout = new System.Windows.Forms.Button();
            this.AllItems = new System.Windows.Forms.Button();
            this.SellItem = new System.Windows.Forms.Button();
            this.AddItem = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ViewSalesMen);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.SoldItems);
            this.panel2.Controls.Add(this.MonthlyProfit);
            this.panel2.Controls.Add(this.TodaysProfit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(258, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(244, 388);
            this.panel2.TabIndex = 9;
            this.panel2.Visible = false;
            // 
            // ViewSalesMen
            // 
            this.ViewSalesMen.Location = new System.Drawing.Point(51, 240);
            this.ViewSalesMen.Name = "ViewSalesMen";
            this.ViewSalesMen.Size = new System.Drawing.Size(105, 44);
            this.ViewSalesMen.TabIndex = 4;
            this.ViewSalesMen.Text = "View Salesmen";
            this.ViewSalesMen.UseVisualStyleBackColor = true;
            this.ViewSalesMen.Click += new System.EventHandler(this.ViewSalesMen_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(51, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 42);
            this.button1.TabIndex = 3;
            this.button1.Text = "Add Salesman";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SoldItems
            // 
            this.SoldItems.Location = new System.Drawing.Point(51, 142);
            this.SoldItems.Name = "SoldItems";
            this.SoldItems.Size = new System.Drawing.Size(105, 44);
            this.SoldItems.TabIndex = 2;
            this.SoldItems.Text = "Today\'s Sold Items";
            this.SoldItems.UseVisualStyleBackColor = true;
            this.SoldItems.Click += new System.EventHandler(this.SoldItems_Click);
            // 
            // MonthlyProfit
            // 
            this.MonthlyProfit.Location = new System.Drawing.Point(51, 95);
            this.MonthlyProfit.Name = "MonthlyProfit";
            this.MonthlyProfit.Size = new System.Drawing.Size(105, 41);
            this.MonthlyProfit.TabIndex = 1;
            this.MonthlyProfit.Text = "Check Monthly Profit";
            this.MonthlyProfit.UseVisualStyleBackColor = true;
            this.MonthlyProfit.Click += new System.EventHandler(this.MonthlyProfit_Click);
            // 
            // TodaysProfit
            // 
            this.TodaysProfit.Location = new System.Drawing.Point(51, 45);
            this.TodaysProfit.Name = "TodaysProfit";
            this.TodaysProfit.Size = new System.Drawing.Size(105, 44);
            this.TodaysProfit.TabIndex = 0;
            this.TodaysProfit.Text = "Check Today\'s Profit";
            this.TodaysProfit.UseVisualStyleBackColor = true;
            this.TodaysProfit.Click += new System.EventHandler(this.TodaysProfit_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Logout);
            this.panel1.Controls.Add(this.AllItems);
            this.panel1.Controls.Add(this.SellItem);
            this.panel1.Controls.Add(this.AddItem);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(259, 388);
            this.panel1.TabIndex = 8;
            this.panel1.Visible = false;
            // 
            // Logout
            // 
            this.Logout.Location = new System.Drawing.Point(21, 12);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(75, 23);
            this.Logout.TabIndex = 3;
            this.Logout.Text = "Log Out";
            this.Logout.UseVisualStyleBackColor = true;
            this.Logout.Click += new System.EventHandler(this.Logout_Click);
            // 
            // AllItems
            // 
            this.AllItems.Location = new System.Drawing.Point(56, 192);
            this.AllItems.Name = "AllItems";
            this.AllItems.Size = new System.Drawing.Size(101, 43);
            this.AllItems.TabIndex = 2;
            this.AllItems.Text = "All Items";
            this.AllItems.UseVisualStyleBackColor = true;
            this.AllItems.Click += new System.EventHandler(this.AllItems_Click);
            // 
            // SellItem
            // 
            this.SellItem.Location = new System.Drawing.Point(56, 144);
            this.SellItem.Name = "SellItem";
            this.SellItem.Size = new System.Drawing.Size(101, 40);
            this.SellItem.TabIndex = 1;
            this.SellItem.Text = "Sell Item";
            this.SellItem.UseVisualStyleBackColor = true;
            this.SellItem.Click += new System.EventHandler(this.SellItem_Click);
            // 
            // AddItem
            // 
            this.AddItem.Location = new System.Drawing.Point(56, 95);
            this.AddItem.Name = "AddItem";
            this.AddItem.Size = new System.Drawing.Size(101, 43);
            this.AddItem.TabIndex = 0;
            this.AddItem.Text = "Add Item";
            this.AddItem.UseVisualStyleBackColor = true;
            this.AddItem.Click += new System.EventHandler(this.AddItem_Click);
            // 
            // home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 388);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "home";
            this.Text = "home";
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button ViewSalesMen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button SoldItems;
        private System.Windows.Forms.Button MonthlyProfit;
        private System.Windows.Forms.Button TodaysProfit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Logout;
        private System.Windows.Forms.Button AllItems;
        private System.Windows.Forms.Button SellItem;
        private System.Windows.Forms.Button AddItem;
    }
}