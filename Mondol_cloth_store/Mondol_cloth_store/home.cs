﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondol_cloth_store
{
    public partial class home : Form
    {
        public home(int i)
        {
            InitializeComponent();
            if (i == 1)
            {
                panel2.Visible = true; 
            }
            panel1.Visible = true;
        }

        private void TodaysProfit_Click(object sender, EventArgs e)
        {

        }

        private void MonthlyProfit_Click(object sender, EventArgs e)
        {

        }

        private void SoldItems_Click(object sender, EventArgs e)
        {
            var si = new solditems();
            si.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var sm = new salesman();
            sm.ShowDialog();
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            var l = new login();
            l.ShowDialog();
            this.Close();
        }

        private void ViewSalesMen_Click(object sender, EventArgs e)
        {
            var vs = new viewSalesman();
            vs.ShowDialog();
        }

        private void AddItem_Click(object sender, EventArgs e)
        {
            var ai = new addItem();
            ai.ShowDialog();
        }

        private void SellItem_Click(object sender, EventArgs e)
        {
            var si = new sellItems();
            si.ShowDialog();
        }

        private void AllItems_Click(object sender, EventArgs e)
        {
            var allItemsForm = new allItems();
            allItemsForm.ShowDialog();
        }
    }
}
