﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondol_cloth_store
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            var username = Username.Text;
            var password = Password.Text;

            if(username == "admin" && password == "mondol@12345"){
                this.Hide();
                var h = new home(1);
                h.ShowDialog();
                this.Close();
            }
            else if (username == "salesman" && password == "sales123")
            {
                this.Hide();
                var h = new home(2);
                h.ShowDialog();
                this.Close();
            }
            else
            {
                Message.Text = "Invalid username or password...";
            }

            Username.Text = "";
            Password.Text = "";
        }

       
    }
}
