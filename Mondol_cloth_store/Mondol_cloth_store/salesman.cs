﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Mondol_cloth_store
{
    public partial class salesman : Form
    {
        public salesman()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = nametext.Text;
            var address = addresstext.Text;
            var mobile = mobiletext.Text;

            DbConnect db = new DbConnect();
            string query = "INSERT INTO mcs_salesman (name, address, mobile) VALUES('" + name + "', '" + address + "', '" + mobile + "')";

            //open connection
            if (db.connect() == true)
            {
                var conn = db.getDB();
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, conn);

                //Execute command
                cmd.ExecuteNonQuery();
            }

            MessageBox.Show("Item added successfully...");
            this.Close();
        }
    }
}
