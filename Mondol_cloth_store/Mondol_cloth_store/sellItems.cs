﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Mondol_cloth_store
{
    public partial class sellItems : Form
    {
        DbConnect db;
        double total=0.0;
        public sellItems()
        {
            db = new DbConnect();
            InitializeComponent();
        }

        private void sellItems_Load(object sender, EventArgs e)
        {
            
            if (db.connect() == true)
            {
                var conn = db.getDB();
                //create command and assign the query and connection from the constructor
                MySqlCommand mySQLCommand = new MySqlCommand("select * from mcs_item", conn);
                var reader = mySQLCommand.ExecuteReader();

                while(reader.Read()){
                    string name = reader.GetString("name");
                    comboBox1.Items.Add(name);
                }
                reader.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var conn = db.getDB();
            var id = comboBox1.Text;
            var sql = "select * from mcs_item where name = '" + id + "'";
            //create command and assign the query and connection from the constructor
            MySqlCommand mySQLCommand = new MySqlCommand(sql, conn);
            var reader = mySQLCommand.ExecuteReader();
            string name = "";
            double price = 0.0;
            int qua = 0;
            double totali = 0.0;
            double sellprice = 0.0;
            while (reader.Read())
            {
                name = reader.GetString("name");
                price = double.Parse(reader.GetString("price"));
                qua = int.Parse(quantity.Text);
                sellprice = double.Parse(textBox1.Text);
                totali = qua*sellprice;
                DataGridViewRow row = (DataGridViewRow)dataGridView1.RowTemplate.Clone();
                row.CreateCells(dataGridView1, name, sellprice, qua, totali);
                total += totali;
                textBox2.Text = total + "";

                dataGridView1.Rows.Add(row);
            }

            reader.Close();

            string query = "INSERT INTO mcs_sold_item (name, price, sell_price, quantity, date) VALUES('" + name + "', '" + price + "', '" + sellprice + "', '" + qua + "', CURDATE())";
            
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.ExecuteNonQuery();
        }
    }
}
