﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace Mondol_cloth_store
{
    public partial class solditems : Form
    {
        public solditems()
        {
            InitializeComponent();
        }

        private void solditems_Load(object sender, EventArgs e)
        {
            DbConnect db = new DbConnect();
            if (db.connect() == true)
            {
                var conn = db.getDB();
                //create command and assign the query and connection from the constructor
                var mySqlDataAdapter = new MySqlDataAdapter("select name, price, avg(sell_price) as average_sell_price, sum(quantity) as quantity from mcs_sold_item where date = CURDATE() group by name", conn);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);
                dataGridView1.DataSource = DS.Tables[0];
            }
        }
    }
}
