﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondol_cloth_store
{
    public partial class viewSalesman : Form
    {
        public viewSalesman()
        {
            InitializeComponent();
        }

        private void viewSalesman_Load(object sender, EventArgs e)
        {
            DbConnect db = new DbConnect();
            if (db.connect() == true)
            {
                var conn = db.getDB();
                //create command and assign the query and connection from the constructor
                var mySqlDataAdapter = new MySqlDataAdapter("select name, address, mobile from mcs_salesman", conn);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);
                dataGridView1.DataSource = DS.Tables[0];
            }
        }
    }
}
